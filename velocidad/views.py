# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, HttpResponseRedirect, HttpResponse, render
from django.contrib.auth import authenticate, login, logout
from django.template import RequestContext
from django.db.models import *
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required, permission_required
from django.template.loader import render_to_string
from datetime import datetime
import json
from django.conf import settings
import os
from django.db.models import Q
import math

from stravalib.client import Client

# Create your views here.
def home(request):
    client = Client()
    authorize_url = client.authorization_url(client_id=12917, redirect_uri='http://localhost:8000/authorization')
    print authorize_url

    if 'code' in request.GET:
        code = request.GET['code'] # or whatever your framework does
        print "---------------"
        print code
        access_token = client.exchange_code_for_token(client_id=12917, client_secret='9097d8a9c0ff6607101d0fb2d5a87311c351b6f3 ', code=code)

        # Now store that access token somewhere (a database?)
        client.access_token = access_token
        athlete = client.get_athlete()
        print("For {id}, I now have an access token {token}".format(id=athlete.id, token=access_token))
    # Have the user click the authorization URL, a 'code' param will be added to the redirect_uri
    # .....
    return render(request, 'home.html', locals(), context_instance=RequestContext(request))

def autorizado(request):
    client = Client()
    authorize_url = client.authorization_url(client_id=12917, redirect_uri='http://localhost:8000/authorization')
    print authorize_url
    access_token = "e448e0d22b731c884c43430c963bdbf61ec4e468 "
    client.access_token = "e448e0d22b731c884c43430c963bdbf61ec4e468 "
    athlete = client.get_athlete()
    print("For {id}, I now have an access token {token}".format(id=athlete.id, token=access_token))
    # Have the user click the authorization URL, a 'code' param will be added to the redirect_uri
    # activity = client.get_activity(658646051)

        # Activities can have many streams, you can request n desired stream types
    types = ['latlng', 'altitude','velocity_smooth', 'grade_smooth']

    # streams = client.get_activity_streams(534909249, types=types, resolution='low') # Paipa
    streams = client.get_activity_streams(677139380, types=types) # Tourmalet Chivata
    # streams = client.get_activity_streams(677139380, types=['altitude'], resolution='low') # Tourmalet Chivata

    # streams = client.get_segment_streams(10104565, types=types, resolution='medium') # Parte dura tourmalet
    # streams = client.get_segment_streams(5580054, types=types, resolution='low') #Avenida universitaria

    altitude = streams['altitude'].data
    latlng = streams['latlng'].data
    intervals = 10
    organized = processDataAltitude(streams,300,intervals)
    return render(request, 'autorizado.html', locals(), context_instance=RequestContext(request))

def processDataAltitude(data, hsvColor, intervals):
    minimun_alt = 10000
    maximun_alt = 0
    minimun_vel = 10000
    maximun_vel = 0
    minimun_grade = 101
    maximun_grade = -101

    for index, latlng in enumerate(data['latlng'].data):
        minimun_alt = min(data['altitude'].data[index], minimun_alt)
        maximun_alt = max(data['altitude'].data[index], maximun_alt)
        minimun_vel = min(data['velocity_smooth'].data[index], minimun_vel)
        maximun_vel = max(data['velocity_smooth'].data[index], maximun_vel)
        minimun_grade = min(data['grade_smooth'].data[index], minimun_grade)
        maximun_grade = max(data['grade_smooth'].data[index], maximun_grade)

    range_alt = maximun_alt - minimun_alt
    range_vel = maximun_vel - minimun_vel
    range_grade = maximun_grade - minimun_grade

    res = []

    color_intervals = hsvColor / intervals
    for index, latlng in enumerate(data['latlng'].data):
        alt = data['altitude'].data[index]
        vel = data['velocity_smooth'].data[index]
        grade = data['grade_smooth'].data[index]

        r_alt = (alt-minimun_alt) * 100 / range_alt
        percentage_alt = r_alt * (hsvColor/100)

        r_vel = (vel-minimun_vel) * 100 / range_vel
        percentage_vel = r_vel * (hsvColor/100)

        r_grade = (grade-minimun_grade) * 100 / range_grade
        percentage_grade = r_grade * (hsvColor/100)
        aux = {}

        aux['latlng'] = latlng
        aux['data_altitude'] = {'altitude':alt, 'color':str(abs(int(percentage_alt/color_intervals)-intervals))}
        aux['data_velocity'] = {'altitude':vel, 'color':str(int(percentage_vel/color_intervals))}
        aux['data_grade'] = {'altitude':grade, 'color':str(abs(int(percentage_grade/color_intervals)-intervals))}
        res.append(aux)
    return res;

def processDataVelocity(data, hsvColor, intervals):
    minimun_vel = 10000
    maximun_vel = 0

    for vel in data['velocity_smooth'].data:
        minimun_vel = min(vel, minimun_vel)
        maximun_vel = max(vel, maximun_vel)

    range_vel = maximun_vel - minimun_vel

    res = []

    color_intervals = hsvColor / intervals
    for index, vel in enumerate(data['velocity_smooth'].data):
        r = (vel-minimun_vel) * 100 / range_vel
        percentage = r * (hsvColor/100)
        aux = {}
        aux['latlng'] = data['latlng'].data[index]
        aux['velocity_smooth'] = vel
        aux['color'] = str(int(percentage/color_intervals))
        res.append(aux)
    return res;

def processDataGrade(data, hsvColor, intervals):
    minimun_grade = 10000
    maximun_grade = 0

    for vel in data['grade_smooth'].data:
        minimun_grade = min(vel, minimun_grade)
        maximun_grade = max(vel, maximun_grade)

    range_grade = maximun_grade - minimun_grade

    res = []

    color_intervals = hsvColor / intervals
    for index, vel in enumerate(data['grade_smooth'].data):
        r = (vel-minimun_grade) * 100 / range_grade
        percentage = r * (hsvColor/100)
        aux = {}
        aux['latlng'] = data['latlng'].data[index]
        aux['grade_smooth'] = vel
        aux['color'] = str(abs(int(percentage/color_intervals)-intervals))
        res.append(aux)
    return res;
