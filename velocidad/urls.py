from django.conf.urls import url, include
from django.contrib import admin
import velocidad.views

urlpatterns = [
    url(r'^$', velocidad.views.home, name='home'),
    url(r'authorization/$', velocidad.views.autorizado, name='autorizado'),
]
