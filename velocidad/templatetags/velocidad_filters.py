from django import template
register = template.Library()

@register.filter
def list_nex_lat(value, index):
	index = (index+1)%len(value)
	return value[index]['latlng'][0]

@register.filter
def list_nex_lng(value, index):
	index = (index+1)%len(value)
	return value[index]['latlng'][1]